from django.core.management.base import BaseCommand
from django.contrib.auth.models import User

import django
import datetime

django.setup()

from data.models import Category, Workflow
from django.core.exceptions import ObjectDoesNotExist

#models
CATEGORY = 'category'
USER = 'user'
WORKFLOW = 'workflow'

# The name of this class is not optional must  be Command
# otherwise manage.py will not process it properly
class Command(BaseCommand):
    #  args = '<-no arguments>'
    # helps and arguments shown when command python manage.py help populate
    # is executed.
    help = 'This scripts do some task in the database, no arguments needed.' \
           'Execute it with the command line python manage.py query_data'

    def getParragraph(self, init, end):
        # getParragraph returns a parragraph, useful for testing
        if end > 445:
            end = 445
        if init < 0:
            init = 0
        return """Lorem ipsum dolor sit amet, consectetur adipiscing elit,
sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris
nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in
reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia
deserunt mollit anim id est laborum."""[init:end]

    # handle is another compulsory name, This function will be
    # executed by default
    def handle(self, *args, **options):
        self.cleanDatabase()
        c1 = Category.objects.get_or_create(name="category 1")[0]
        c1.save()
        c2 = Category.objects.get_or_create(name="category 2")[0]
        c2.save()

        w11 = Workflow(name="workflow 11")
        w11.save()
        w11.category.add(c1)
        w12 = Workflow(name="workflow 12")
        w12.save()
        w12.category.add(c1)
        w13 = Workflow(name="workflow 13")
        w13.save()
        w13.category.add(c1)

        w21 = Workflow(name="workflow 21")
        w21.save()
        w21.category.add(c2)
        w22 = Workflow(name="workflow 22")
        w22.save()
        w22.category.add(c2)
        w23 = Workflow(name="workflow 23")
        w23.save()
        w23.category.add(c2)

        q1 = Workflow.objects.filter(category=Category.objects.get(name = "category 1"))
        print "Resultado de la primera consulta:"
        print q1

        q2 = Workflow.objects.filter(slug__contains="workflow-1")

        q3 = q2.all()[0]
        category = Category.objects.get(name=q3.category.all()[0])
        print "Resultado de la segunda consulta:"
        print category.slug

        try:
            q4 = Workflow.objects.get(name="workflow_10")

        except ObjectDoesNotExist:
            print "Resultado de la tercera consulta:"
            print "workflow"+" \"workflow_10\" "+"inexistente"

    def cleanDatabase(self):
        # delete all
        # workflows and  categories
        # ADD CODE HERE
        c = Category.objects.all()
        c.delete()
        w = Workflow.objects.all()
        w.delete()
        pass